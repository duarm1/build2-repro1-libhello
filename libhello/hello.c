#include <libhello/hello.h>

#include <errno.h>
#include <stdio.h>

int say_hello(FILE *f, const char *n) {
  if (f == NULL || n == NULL || *n == '\0') {
    errno = EINVAL;
    return -1;
  }

#ifdef HELLO_DEBUG
  printf("AAAA");
#endif

  return fprintf(f, "Hello, %s!\n", n);
}
